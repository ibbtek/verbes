/*
* The MIT License
*
* Verbes
*
* Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package com.ibbtek.print;

import static com.ibbtek.utilities.LogToFile.log;
import java.io.IOException;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import org.w3c.dom.NodeList;

/**
 * PrintVerb class
 * @author Ibbtek <http://ibbtek.altervista.org/>
 */
public class PrintVerb {
    
    private final JTextPane jTextPane;
    private final NodeList nlConj;
    private final String verb;
    private final String tmpl;
    private final String end;
    private final String begin;
    /**
     * PrintVerb constructor
     * @param jTextPane
     * @param nlConj
     * @param verb
     * @param tmpl 
     */
    public PrintVerb(JTextPane jTextPane, NodeList nlConj, String verb, String tmpl){
        
        this.end=tmpl.substring(tmpl.lastIndexOf(":")+1);
        this.begin = verb.substring(0, verb.length()-end.length());
        this.jTextPane=jTextPane;
        this.nlConj=nlConj;
        this.verb=verb;
        this.tmpl=tmpl;
        printVerb();
    }
    /**
     * printVerb Method
     * Print the verb selectionned
     */
    private void printVerb(){
        /*
            Clear the verbTextPane
        */
        jTextPane.setText("");
        /*
            Create an HTML document
        */
        HTMLDocument doc = (HTMLDocument)jTextPane.getDocument();
        HTMLEditorKit editorKit =
                (HTMLEditorKit)jTextPane.getEditorKit();
        /*
            Call the conjHtml method and retrieve the text
            Pass conjConvert Method in argument
        */
        String text = conjHtml(conjConvert());
        /*
            Insert Text in HTML document
        */
        try {
            editorKit.insertHTML(doc, doc.getLength(), text, 0, 0, null);
        } catch (BadLocationException | IOException ex) {
            log(ex,"severe","Can't insert HTML while printing Verb");
            return;
        }
        /*
            Set caret position to 0
        */
        jTextPane.setCaretPosition(0);
    }
    /**
     * conjConvert Method
     * Convert the retrieved data from the xml file
     * and set it in a String[] after adding some tags
     * @return String of the converted text
     */
    private String[] conjConvert(){
        
        String conj[]=new String[51];
        
        for(int i=0; i<=50;i++){
            if(!nlConj.item(i).getTextContent().equals(""))
                conj[i]=begin + "<span style=\"color:#FF5050;\">" + nlConj.item(i).getTextContent()+ "</span>";
            else
                conj[i]="-";
        }
        return conj;
    }
    /**
     * Determine the First Singular Person Pronom (j' or je)
     * @param word
     * @return 
     */
    private String pps(String word){
        char c;
        if(word.charAt(0)=='<')
            c=word.charAt(29);
        else c=word.charAt(0);
        
        if(c!='a' && c!='e' && c!='i' && c!='o' && c!='u' && c!='y')
            return"je ";
        else return"j'";
    }
    /**
     * conjHtml Method
     * Construct the String html page
     * @param conj String[]
     * @return String
     */
    private String conjHtml(String[] conj){
        /*
            This will construct the html string
            put it in form and insert the data
            at the appropriate position
        */
        String htmlConjug;
        htmlConjug = ""
                + "<html>\n"
                + "  <head>\n"
                + "    <title>Conjugaison</title>\n"
                + "  </head>\n"
                + "  <body>\n"
                + "    <p style=\"text-align: center;font-size:30px;background-color:#ADADFF;\"> "
                + "<strong>" + Character.toUpperCase(verb.charAt(0)) + verb.substring(1) + "</strong></p>\n"
                + "    <table width=\"100%\">\n"
                + "      <tbody>\n"
                + "        <tr>\n"
                + "          <td class=\"mode\" colspan=\"4\" style=\"background-color:#E6E6FA;\"> <strong>Indicatif</strong></td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
                + "            " + pps(conj[1]) + conj[1] + "<br>\n"
                + "            tu " + conj[2] + "<br>\n"
                + "            il " + conj[3] + "<br>\n"
                + "            nous " + conj[4] + "<br>\n"
                + "            vous " + conj[5] + "<br>\n"
                + "            ils " + conj[6] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Imparfait</u></b><br>\n"
                + "            " + pps(conj[7]) + conj[7] + "<br>\n"
                + "            tu " + conj[8] + "<br>\n"
                + "            il " + conj[9] + "<br>\n"
                + "            nous " + conj[10] + "<br>\n"
                + "            vous " + conj[11] + "<br>\n"
                + "            ils " + conj[12] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé simple</u></b><br>\n"
                + "            " + pps(conj[19]) + conj[19] + "<br>\n"
                + "            tu " + conj[20] + "<br>\n"
                + "            il " + conj[21] + "<br>\n"
                + "            nous " + conj[22] + "<br>\n"
                + "            vous " + conj[23] + "<br>\n"
                + "            ils " + conj[24] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Futur simple</u></b><br>\n"
                + "            " + pps(conj[13]) + conj[13] + "<br>\n"
                + "            tu " + conj[14] + "<br>\n"
                + "            il " + conj[15] + "<br>\n"
                + "            nous " + conj[16] + "<br>\n"
                + "            vous " + conj[17] + "<br>\n"
                + "            ils " + conj[18] + "</td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "          <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Subjonctif</strong></td>\n"
                + "          <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Conditionnel</strong></td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
                + "            que " + pps(conj[31]) + conj[31] + "<br>\n"
                + "            que tu " + conj[32] + "<br>\n"
                + "            qu'il " + conj[33] + "<br>\n"
                + "            que nous " + conj[34] + "<br>\n"
                + "            que vous " + conj[35] + "<br>\n"
                + "            qu'ils " + conj[36] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Imparfait</u></b><br>\n"
                + "            que " + pps(conj[37]) + conj[37] + "<br>\n"
                + "            que tu " + conj[38] + "<br>\n"
                + "            qu'il " + conj[39] + "<br>\n"
                + "            que nous " + conj[40] + "<br>\n"
                + "            que vous " + conj[41] + "<br>\n"
                + "            qu'ils " + conj[42] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
                + "            " + pps(conj[25]) + conj[25] + "<br>\n"
                + "            tu " + conj[26] + "<br>\n"
                + "            il " + conj[27] + "<br>\n"
                + "            nous " + conj[28] + "<br>\n"
                + "            vous " + conj[29] + "<br>\n"
                + "            ils " + conj[30] + "</td>\n"
                + "          <td> <br>\n"
                + "          </td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "          <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Participe</strong></td>\n"
                + "          <td class=\"mode\" colspan=\"1\" bgcolor=\"#E6E6FA\"> <strong>Impératif</strong></td>\n"
                + "          <td class=\"mode\" colspan=\"1\" bgcolor=\"#E6E6FA\"> <strong>Infinitif</strong></td>\n"
                + "        </tr>\n"
                + "        <tr>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
                + "            " + conj[46] + "</b></td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé</u></b><br>\n"
                + "            " + conj[47] + "<br>\n"
                + "            " + conj[48] + "<br>\n"
                + "            " + conj[49] + "<br>\n"
                + "            " + conj[50] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
                + "            " + conj[43] + "<br>\n"
                + "            " + conj[44] + "<br>\n"
                + "            " + conj[45] + "</td>\n"
                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
                + "            " + conj[0] + "</td>\n"
                + "        </tr>\n"
                + "      </tbody>\n"
                + "    </table>\n"
                + "    <p> </p>\n"
                + "  </body>\n"
                + "</html>";
        /*
            This is for eventuel further purpose
            I plan to be able to get the composed form
            of the verbs
        */
//////        htmlConjug = ""
//////                + "<html>\n"
//////                + "  <head>\n"
//////                + "    <title>Conjugaison</title>\n"
//////                + "  </head>\n"
//////                + "  <body>\n"
//////                + "    <p style=\"text-align: center;font-size:30px;background-color:#ADADFF;\"> "
//////                + "<strong>" + Character.toUpperCase(verb.charAt(0)) + verb.substring(1) + "</strong></p>\n"
//////                + "    <p> </p>\n"
//////                + "    <table width=\"100%\">\n"
//////                + "      <tbody>\n"
//////                + "        <tr>\n"
//////                + "          <td class=\"mode\" colspan=\"4\" style=\"background-color:#E6E6FA;\"> <strong>Indicatif</strong></td>\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            " + pps + conj[1] + "<br>\n"
//////                + "            tu " + conj[2] + "<br>\n"
//////                + "            il " + conj[3] + "<br>\n"
//////                + "            nous " + conj[4] + "<br>\n"
//////                + "            vous " + conj[5] + "<br>\n"
//////                + "            ils " + conj[6] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé composé</u></b><br>\n"
//////                + "            j'ai " + begin + "<b>é</b><br>\n"
//////                + "            tu as " + begin + "<b>é</b><br>\n"
//////                + "            il a " + begin + "<b>é</b><br>\n"
//////                + "            nous avons " + begin + "<b>é</b><br>\n"
//////                + "            vous avez " + begin + "<b>é</b><br>\n"
//////                + "            ils ont " + begin + "<b>é</b></td> -->\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Imparfait</u></b><br>\n"
//////                + "            " + pps + conj[7] + "<br>\n"
//////                + "            tu " + conj[8] + "<br>\n"
//////                + "            il " + conj[9] + "<br>\n"
//////                + "            nous " + conj[10] + "<br>\n"
//////                + "            vous " + conj[11] + "<br>\n"
//////                + "            ils " + conj[12] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Plus-que-parfait</u></b><br>\n"
//////                + "            j'avais " + begin + "<b>é</b><br>\n"
//////                + "            tu avais " + begin + "<b>é</b><br>\n"
//////                + "            il avait " + begin + "<b>é</b><br>\n"
//////                + "            nous avions " + begin + "<b>é</b><br>\n"
//////                + "            vous aviez " + begin + "<b>é</b><br>\n"
//////                + "            ils avaient " + begin + "<b>é</b></td> -->\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé simple</u></b><br>\n"
//////                + "            " + pps + conj[19] + "<br>\n"
//////                + "            tu " + conj[20] + "<br>\n"
//////                + "            il " + conj[21] + "<br>\n"
//////                + "            nous " + conj[22] + "<br>\n"
//////                + "            vous " + conj[23] + "<br>\n"
//////                + "            ils " + conj[24] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé antérieur</u></b><br>\n"
//////                + "            j'eus " + begin + "<b>é</b><br>\n"
//////                + "            tu eus " + begin + "<b>é</b><br>\n"
//////                + "            il eut " + begin + "<b>é</b><br>\n"
//////                + "            nous eûmes " + begin + "<b>é</b><br>\n"
//////                + "            vous eûtes " + begin + "<b>é</b><br>\n"
//////                + "            ils eurent " + begin + "<b>é</b></td> -->\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Futur simple</u></b><br>\n"
//////                + "            " + pps + conj[13] + "<br>\n"
//////                + "            tu " + conj[14] + "<br>\n"
//////                + "            il " + conj[15] + "<br>\n"
//////                + "            nous " + conj[16] + "<br>\n"
//////                + "            vous " + conj[17] + "<br>\n"
//////                + "            ils " + conj[18] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Futur antérieur</u></b><br>\n"
//////                + "            j'aurai " + begin + "<b>é</b><br>\n"
//////                + "            tu auras " + begin + "<b>é</b><br>\n"
//////                + "            il aura " + begin + "<b>é</b><br>\n"
//////                + "            nous aurons " + begin + "<b>é</b><br>\n"
//////                + "            vous aurez " + begin + "<b>é</b><br>\n"
//////                + "            ils auront " + begin + "<b>é</b></td> -->\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td class=\"mode\" colspan=\"4\" bgcolor=\"#E6E6FA\"> <strong>Subjonctif</strong></td>\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            que " + pps + conj[31] + "<br>\n"
//////                + "            que tu " + conj[32] + "<br>\n"
//////                + "            qu'il " + conj[33] + "<br>\n"
//////                + "            que nous " + conj[34] + "<br>\n"
//////                + "            que vous " + conj[35] + "<br>\n"
//////                + "            qu'ils " + conj[36] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé</u></b><br>\n"
//////                + "            que j'aie " + begin + "<b>é</b><br>\n"
//////                + "            que tu aies " + begin + "<b>é</b><br>\n"
//////                + "            qu'il ait " + begin + "<b>é</b><br>\n"
//////                + "            que nous ayons " + begin + "<b>é</b><br>\n"
//////                + "            que vous ayez " + begin + "<b>é</b><br>\n"
//////                + "            qu'ils aient " + begin + "<b>é</b></td> -->\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Imparfait</u></b><br>\n"
//////                + "            que " + pps + conj[37] + "<br>\n"
//////                + "            que tu " + conj[38] + "<br>\n"
//////                + "            qu'il " + conj[39] + "<br>\n"
//////                + "            que nous " + conj[40] + "<br>\n"
//////                + "            que vous " + conj[41] + "<br>\n"
//////                + "            qu'ils " + conj[42] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Plus-que-parfait</u></b><br>\n"
//////                + "            que j'eusse " + begin + "<b>é</b><br>\n"
//////                + "            que tu eusses " + begin + "<b>é</b><br>\n"
//////                + "            qu'il eût " + begin + "<b>é</b><br>\n"
//////                + "            que nous eussions " + begin + "<b>é</b><br>\n"
//////                + "            que vous eussiez " + begin + "<b>é</b><br>\n"
//////                + "            qu'ils eussent " + begin + "<b>é</b></td> -->\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td class=\"mode\" colspan=\"4\" bgcolor=\"#E6E6FA\"> <strong>Conditionnel</strong></td>\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            " + pps + conj[25] + "<br>\n"
//////                + "            tu " + conj[26] + "<br>\n"
//////                + "            il " + conj[27] + "<br>\n"
//////                + "            nous " + conj[28] + "<br>\n"
//////                + "            vous " + conj[29] + "<br>\n"
//////                + "            ils " + conj[30] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé première forme</u></b><br>\n"
//////                + "            j'aurais " + begin + "<b>é</b><br>\n"
//////                + "            tu aurais " + begin + "<b>é</b><br>\n"
//////                + "            il aurait " + begin + "<b>é</b><br>\n"
//////                + "            nous aurions " + begin + "<b>é</b><br>\n"
//////                + "            vous auriez " + begin + "<b>é</b><br>\n"
//////                + "            ils auraient " + begin + "<b>é</b></td>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé deuxième forme</u></b><br>\n"
//////                + "            j'eusse " + begin + "<b>é</b><br>\n"
//////                + "            tu eusses " + begin + "<b>é</b><br>\n"
//////                + "            il eût " + begin + "<b>é</b><br>\n"
//////                + "            nous eussions " + begin + "<b>é</b><br>\n"
//////                + "            vous eussiez " + begin + "<b>é</b><br>\n"
//////                + "            ils eussent " + begin + "<b>é</b></td> -->\n"
//////                + "          <td> <br>\n"
//////                + "          </td>\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Participe</strong></td>\n"
//////                + "          <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Impératif</strong></td>\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            " + conj[46] + "</b></td>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé</u></b><br>\n"
//////                + "            " + conj[47] + "<br>\n"
//////                + "            " + conj[48] + "<br>\n"
//////                + "            " + conj[49] + "<br>\n"
//////                + "            " + conj[50] + "<br>\n"
//////                + "            ayant " + begin + "<b>é</b></td>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            " + conj[43] + "<br>\n"
//////                + "            " + conj[44] + "<br>\n"
//////                + "            " + conj[45] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé</u></b><br>\n"
//////                + "            aie " + begin + "<b>é</b><br>\n"
//////                + "            ayons " + begin + "<b>é</b><br>\n"
//////                + "            ayez " + begin + "<b>é</b></td> -->\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Infinitif</strong></td>\n"
//////                + "          <!-- <td class=\"mode\" colspan=\"2\" bgcolor=\"#E6E6FA\"> <strong>Gérondif</strong></td> -->\n"
//////                + "        </tr>\n"
//////                + "        <tr>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            " + conj[0] + "</td>\n"
//////                + "          <!-- <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé</u></b><br>\n"
//////                + "            avoir " + begin + "<b>é</b></td>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Présent</u></b><br>\n"
//////                + "            en " + begin + "<b>ant</b></td>\n"
//////                + "          <td> <b style=\"background-color:#F5F5F9;\"> <u>Passé</u></b><br>\n"
//////                + "            en ayant " + begin + "<b>é</b></td> -->\n"
//////                + "        </tr>\n"
//////                + "      </tbody>\n"
//////                + "    </table>\n"
//////                + "    <p> </p>\n"
//////                + "  </body>\n"
//////                + "</html>";
        /*
            return the html string text
        */
        return htmlConjug;
    }
}
