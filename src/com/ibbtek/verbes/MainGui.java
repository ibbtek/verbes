/*
 * The MIT License
 *
 * Verbes
 *
 * Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.ibbtek.verbes;

import com.ibbtek.listeners.TreeListener;
import com.ibbtek.utilities.FileHandler;
import com.ibbtek.utilities.CopyPaste;
import com.ibbtek.utilities.SerialTree;
import com.ibbtek.utilities.XmlJTree;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Normalizer;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.text.DefaultEditorKit;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author Ibbtek <http://ibbtek.altervista.org/>
 */
public class MainGui extends javax.swing.JFrame {
    public static String verbs;
    public static String conjugation;
    /**
     * Creates new form MainGui
     * @param word
     */
    public MainGui(String word) {
        
        /*
            Initialize the components
        */
        initComponents(); 
//        verbsTree.setModel(new SerialTree(appPath
//                +"/data/verbs/verbsTree.data").getDtModel());
        String internalURL = new FileHandler().
                getInternalURL("/data/verbs/verbsTree.data");
        verbsTree.setModel(new SerialTree(internalURL).getDtModel());
        /*
        Add copy/paste in edit menu
        */
        Action cutAction = new DefaultEditorKit.CutAction();
        cutAction.putValue(Action.NAME, "Cut");
        cutAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("ctrl X"));
        cutAction.putValue(Action.SMALL_ICON,
                new javax.swing.ImageIcon(getClass().
                        getResource("/data/images/cut.png")));
        jMenuEdit.add(cutAction);
        
        Action copyAction = new DefaultEditorKit.CopyAction();
        copyAction.putValue(Action.NAME, "Copy");
        copyAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("ctrl C"));
        copyAction.putValue(Action.SMALL_ICON,
                new javax.swing.ImageIcon(getClass().
                        getResource("/data/images/copy.png")));
        jMenuEdit.add(copyAction);
        
        Action pasteAction = new DefaultEditorKit.PasteAction();
        pasteAction.putValue(Action.NAME, "Paste");
        pasteAction.putValue(Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("ctrl V"));
        pasteAction.putValue(Action.SMALL_ICON,
                new javax.swing.ImageIcon(getClass().
                        getResource("/data/images/paste.png")));
        jMenuEdit.add(pasteAction);
        /*
            Add copy/paste context menu
        */
        Toolkit.getDefaultToolkit().getSystemEventQueue().push(new CopyPaste());
         /*
            Add TreeSelectionListener to verbsTree
        */
        verbsTree.addTreeSelectionListener(
                new TreeListener(verbsTextPane,searchField));
        /*
            Set the icon image of the frame
        */
        this.setIconImage(new ImageIcon(getClass().
                getResource("/data/images/verbes.png")).getImage());
        /*
            Add an ActionListener to the searchField to launch search
            by pressing ENTER button
        */
        searchField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectLatin(verbsTree,
                        searchField.getText().toLowerCase());
            }
        });
        /*
            Create temporary files
        */
        verbs = new FileHandler().getInternalURL("/data/verbs/verbs-fr.xml");
        conjugation = new FileHandler().getInternalURL("/data/verbs/conjugation-fr.xml");
        
        //First word to display
        searchField.setText(word);
        selectLatin(verbsTree,searchField.getText().toLowerCase());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchField = new javax.swing.JTextField();
        searchBT = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        verbsTextPane = new javax.swing.JTextPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        verbsTree = new javax.swing.JTree();
        jButtonBack = new javax.swing.JButton();
        jButtonForward = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuItemExit = new javax.swing.JMenuItem();
        jMenuEdit = new javax.swing.JMenu();
        jMenuAbout = new javax.swing.JMenu();
        jMenuItemHelp = new javax.swing.JMenuItem();
        jMenuItemAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Verbes");

        searchField.setFont(new java.awt.Font("DejaVu Sans", 0, 15)); // NOI18N
        searchField.setMinimumSize(new java.awt.Dimension(44, 44));

        searchBT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/data/images/viewmag.png"))); // NOI18N
        searchBT.setDefaultCapable(false);
        searchBT.setFocusable(false);
        searchBT.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        searchBT.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        searchBT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBTActionPerformed(evt);
            }
        });

        jSplitPane1.setDividerLocation(180);
        jSplitPane1.setDividerSize(5);

        verbsTextPane.setEditable(false);
        verbsTextPane.setContentType("text/html"); // NOI18N
        verbsTextPane.setFont(new java.awt.Font("Arial", 0, 19)); // NOI18N
        verbsTextPane.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jScrollPane1.setViewportView(verbsTextPane);

        jSplitPane1.setRightComponent(jScrollPane1);

        jScrollPane2.setViewportView(verbsTree);

        jSplitPane1.setLeftComponent(jScrollPane2);

        jButtonBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/data/images/back.png"))); // NOI18N
        jButtonBack.setDefaultCapable(false);
        jButtonBack.setFocusable(false);
        jButtonBack.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonBack.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBackActionPerformed(evt);
            }
        });

        jButtonForward.setIcon(new javax.swing.ImageIcon(getClass().getResource("/data/images/forward.png"))); // NOI18N
        jButtonForward.setDefaultCapable(false);
        jButtonForward.setFocusable(false);
        jButtonForward.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonForward.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonForward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonForwardActionPerformed(evt);
            }
        });

        jMenuFile.setText("File");

        jMenuItemExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/data/images/cross.png"))); // NOI18N
        jMenuItemExit.setText("Exit");
        jMenuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemExitActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemExit);

        jMenuBar1.add(jMenuFile);

        jMenuEdit.setText("Edit");
        jMenuBar1.add(jMenuEdit);

        jMenuAbout.setText("About");

        jMenuItemHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/data/images/help.png"))); // NOI18N
        jMenuItemHelp.setText("Help");
        jMenuItemHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHelpActionPerformed(evt);
            }
        });
        jMenuAbout.add(jMenuItemHelp);

        jMenuItemAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/data/images/information.png"))); // NOI18N
        jMenuItemAbout.setText("About");
        jMenuItemAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAboutActionPerformed(evt);
            }
        });
        jMenuAbout.add(jMenuItemAbout);

        jMenuBar1.add(jMenuAbout);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(searchBT)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonBack)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonForward)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchField, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE))
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonBack)
                    .addComponent(searchBT)
                    .addComponent(jButtonForward)
                    .addComponent(searchField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItemExitActionPerformed

    private void jMenuItemHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHelpActionPerformed
        FileHandler browserLauncher = new FileHandler();
        browserLauncher.openURL("/data/help/HELP.html");
    }//GEN-LAST:event_jMenuItemHelpActionPerformed

    private void jMenuItemAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAboutActionPerformed
        new AboutGui().setVisible(true);
    }//GEN-LAST:event_jMenuItemAboutActionPerformed

    private void searchBTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBTActionPerformed
        selectLatin(verbsTree,
                        searchField.getText().toLowerCase());
    }//GEN-LAST:event_searchBTActionPerformed

    private void jButtonBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBackActionPerformed
        int[] previous = verbsTree.getSelectionRows();
            if(previous[0]-1>=0){
                verbsTree.setSelectionRow(previous[0]-1);
                verbsTree.scrollPathToVisible(verbsTree.getSelectionPath());
            }
    }//GEN-LAST:event_jButtonBackActionPerformed

    private void jButtonForwardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonForwardActionPerformed
        int[] next = verbsTree.getSelectionRows();
            if(next[0]+1<verbsTree.getRowCount()){
                verbsTree.setSelectionRow(next[0]+1);
                verbsTree.scrollPathToVisible(verbsTree.getSelectionPath());
            }
    }//GEN-LAST:event_jButtonForwardActionPerformed
    /**
     * selectLatin Method
     * Select the node of a simple latin jTree by giving the text of the node
     * @param tree
     * @param text
     */
    private void selectLatin(JTree tree,String text){
        int charMatch=0;
        TreePath old =null;
        if(tree.getSelectionPath()!=null) {
            old = tree.getSelectionPath().getParentPath();
        }
        // Get Root Node from the model ...
        TreeNode rootNode  = (TreeNode)tree.getModel().getRoot();
        // ... and its path in the tree from the tree.
        TreePath path = new TreePath(rootNode);
        //BestMatch path
        TreePath best = new TreePath(rootNode);
        // Iterate over the roots children
        for(int i=0; i<rootNode.getChildCount(); i++) {
            TreeNode child = rootNode.getChildAt(i);
            if(child.toString().charAt(0)==normalizeChar(text)) {
                path = path.pathByAddingChild(child);
                // Iterate of the project's children (root's grandchildren)
                for(int j=0; j<child.getChildCount(); j++) {
                    TreeNode grandchild = child.getChildAt(j);
                    String grandchildText=grandchild.toString().toLowerCase();
                    grandchildText=(grandchildText.contains(".")) ?
                            grandchildText.substring(
                                    0, grandchildText.indexOf("."))
                            : grandchildText ;
                    if(charMatch(normalizeString(grandchildText),normalizeString(text))>charMatch){
                        charMatch=charMatch(normalizeString(grandchildText),normalizeString(text));
                        best = path.pathByAddingChild(grandchild);
                    }
                }
                if(old!=null)tree.collapsePath(old);
                tree.expandPath(best);
                tree.setSelectionPath(best);
                tree.scrollPathToVisible(best);
                
                //This is to solve the problem of the scrollPathToVisible
                // that is not working properly
                Rectangle bounds = tree.getPathBounds(best);
                // set the height to the visible height to force the node to top 
                bounds.height = tree.getVisibleRect().height;
                tree.scrollRectToVisible(bounds);
                break;
            }
        }
    }
    
    private int charMatch(String word, String keyword){
        int charMatch = 0;
        for(int i=0; i<word.length(); i++){
            if(i<keyword.length()){
                if(word.charAt(i)==keyword.charAt(i)){
                    charMatch = charMatch+1;
                }else{
                    break;
                }
            }
        }
        return charMatch;
    }
    /**
     * normalizeChar Method
     * get rid of any accent on the first character
     * @param word
     * @return char
     */
    private char normalizeChar(String word){
        word = Normalizer.normalize(word, Normalizer.Form.NFD);
        word = word.replaceAll("\\p{M}", "");
        word=word.toLowerCase();
        char c;
        /*
        Exceptions in Le Littré dictionnary
        */
        if(word.charAt(0)=='-')
            c=word.charAt(1);
        else
            c=word.charAt(0);
        /*
        Exceptions in Synonyms
        */
        if(c=='æ') c='a';
        if(c=='œ') c='o';
        
        return c;
    }
    /**
     * normalizeChar Method
     * get rid of any accent on the first character
     * @param word
     * @return char
     */
    private String normalizeString(String word){
        word = Normalizer.normalize(word, Normalizer.Form.NFD);
        word = word.replaceAll("\\p{M}", "");
        word=word.toLowerCase();
        
        return word;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(final String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    //I simply like this set of color
                    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
                    defaults.put("nimbusOrange",defaults.get("nimbusFocus"));
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try{
                    if(args.length == 0){
                        new MainGui("conjuguer").setVisible(true);
                    }else{
                        new MainGui(args[0]).setVisible(true);
                    }
                }catch(Exception ex){}
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBack;
    private javax.swing.JButton jButtonForward;
    private javax.swing.JMenu jMenuAbout;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuEdit;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenuItem jMenuItemAbout;
    private javax.swing.JMenuItem jMenuItemExit;
    private javax.swing.JMenuItem jMenuItemHelp;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JButton searchBT;
    private javax.swing.JTextField searchField;
    private javax.swing.JTextPane verbsTextPane;
    private javax.swing.JTree verbsTree;
    // End of variables declaration//GEN-END:variables
}
