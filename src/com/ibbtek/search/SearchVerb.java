/*
* The MIT License
*
* Verbes
*
* Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package com.ibbtek.search;

import com.ibbtek.print.PrintVerb;
import static com.ibbtek.utilities.LogToFile.log;
import com.ibbtek.verbes.MainGui;
import java.io.IOException;
import javax.swing.JTextPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * SearchVerb class
 * @author Ibbtek <http://ibbtek.altervista.org/>
 */
public final class SearchVerb extends Thread{
    
    private final JTextPane jTextPane;
    private DocumentBuilderFactory factory;
    private DocumentBuilder builder;
    private XPathFactory xPathfactory;
    private XPath xpath;
    private Document docVerbs;
    private Document docConj;
    private final String verb;
    /**
     * SearchVerb constructor
     * @param jTextPane
     * @param verb
     */
    public SearchVerb(JTextPane jTextPane,String verb){
        this.jTextPane=jTextPane;
        this.verb=verb.trim();
    }
    /**
     * findVerb Method
     * Find the verb in the xml file retrieve the data and call the PrintVerb
     * method giving it in parameter the retrieved data.
     */
    @Override
    public void run() {
        //String conj[][][] = new String[6][4][6];
        /*
        Parse verbs and conjugation xml files
        */
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            xPathfactory = XPathFactory.newInstance();
            xpath = xPathfactory.newXPath();
            docVerbs = builder.parse(MainGui.verbs);
            docConj = builder.parse(MainGui.conjugation);
            
        } catch (ParserConfigurationException | SAXException |
                IOException ex) {
            log(ex,"severe","Can't parse Quran xml file");
            Thread.currentThread().interrupt();
        }
        /*
        Create NodeLsit verb and conj
        */
        NodeList nlVerb = null;
        NodeList nlConj = null;
        /*
        Search the verb by using XPath expression and retrieve the data
        */
        try {
            XPathExpression xpVerb = xpath.compile("//t[../i='" + verb + "']");
            nlVerb = (NodeList) xpVerb.evaluate(
                    docVerbs, XPathConstants.NODESET);
            if (nlVerb.getLength() > 0) {
                
                XPathExpression xpConj = xpath.compile("//template[@name='"
                        + nlVerb.item(0).getTextContent()+ "']/*/*/p");
                nlConj = (NodeList) xpConj.evaluate(
                        docConj, XPathConstants.NODESET);
            }
        } catch (XPathExpressionException |
                DOMException ex) {
            log(ex,"severe","Can't compile the XPathExpression");
            Thread.currentThread().interrupt();
        }
        /*
        Check if there is any result & send the data to the PrintVerb method
        */
        if (nlConj!=null && nlVerb!=null) {
            PrintVerb print= new PrintVerb(jTextPane,nlConj,verb,
                    nlVerb.item(0).getTextContent());
        }
        Thread.currentThread().interrupt();
    }
}
