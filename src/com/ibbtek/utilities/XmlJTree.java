/*
* The MIT License
*
* Verbes
*
* Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package com.ibbtek.utilities;

import static com.ibbtek.utilities.LogToFile.log;
import java.io.IOException;
import java.text.Normalizer;
import javax.swing.JTree;
import javax.swing.tree.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;


/**
 * XmlJTree class
 * @author Ibbtek <http://ibbtek.altervista.org/>
 */
public final class XmlJTree extends JTree{
    
    DefaultTreeModel dtModel=null;
    
    /**
     * XmlJTree constructor
     * @param filePath
     * @param model
     */
    public XmlJTree(String filePath){
        Node root = null;
        /*
            Parse the xml file
        */
        try {
            DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(filePath);
            root = (Node) doc.getDocumentElement();
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            log(ex,"severe","Can't parse the xml file to construct the tree");
            return;
        }
        /*
            if any result set the appropriate model to the table
        */
        if(root!=null){
            dtModel= new DefaultTreeModel(verbsTreeNode(root));
            this.setModel(dtModel);
        }
    }
    /**
     * verbsTreeNde Method
     * construct the jTree from the verbs xml file
     * @param root
     * @return DefaultMutableTreeNode
     */
    private DefaultMutableTreeNode verbsTreeNode(Node root){
        DefaultMutableTreeNode dmtNode;
        dmtNode=new DefaultMutableTreeNode("Verbes");
        NodeList nodeList=root.getChildNodes();
        char c='a';
            DefaultMutableTreeNode a=new DefaultMutableTreeNode(c);
            dmtNode.add(a);
            for (int count = 0; count < nodeList.getLength(); count++){
                
                Node tempNode = nodeList.item(count);

                if (tempNode.getNodeType() == Node.ELEMENT_NODE){
                    char c2=normalizeString(
                            tempNode.getFirstChild().getTextContent());
                    if(c2==c){
                        a.add(new DefaultMutableTreeNode(
                                tempNode.getFirstChild().getTextContent()));
                    }else{
                        c++;
                        a=new DefaultMutableTreeNode(c);
                        dmtNode.add(a);
                        if(c2==c){
                            a.add(new DefaultMutableTreeNode(
                                    tempNode.getFirstChild().getTextContent()));
                        }
                    }
                }
            }
        return dmtNode;
    }
    /**
     * normalizeString Method
     * get rid of any accent on the first character 
     * @param word
     * @return char
     */
    private char normalizeString(String word){
        word = Normalizer.normalize(word, Normalizer.Form.NFD);
        word = word.replaceAll("\\p{M}", "");
        char c=word.charAt(0);
        return c;
    }
    
    /**
     * defaultTreeNode Method
     * construct simple jTree from any simple xml file
     * @param root
     * @return DefaultMutableTreeNode
     */
    private DefaultMutableTreeNode defaultTreeNode(Node root){
        
        DefaultMutableTreeNode dmtNode;
        try {
            dmtNode = new DefaultMutableTreeNode(root.getNodeName());
            NodeList nodeList = root.getChildNodes();
            for (int count = 0; count < nodeList.getLength(); count++) {
                Node tempNode = nodeList.item(count);
                // make sure it's element node.
                if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (tempNode.hasChildNodes()) {
                        // loop again if has child nodes
                        dmtNode.add(defaultTreeNode(tempNode));
                    }
                }
            }
        } catch (Exception ex) {
            log(ex,"fine","Problem with the DefaultTreeNode");
            return fullTreeNode(root);
        }
        return dmtNode;
    }
    
    /**
     * fullTreeNode Method
     * construct the full jTree from any simple or complex xml file
     * this method is recursive
     * @param root
     * @return DefaultMutableTreeNode
     */
    private DefaultMutableTreeNode fullTreeNode(Node root){
        DefaultMutableTreeNode dmtNode;
        
        String name = root.getNodeName();
        String value = root.getNodeValue();
        
        //Special case for TEXT_NODE,others are similar but not catered for here.
        dmtNode = new DefaultMutableTreeNode(
                root.getNodeType() == Node.TEXT_NODE ? value : name );
        
        // If there are any children and they are non-null then recurse...
        if(root.hasChildNodes()){
            NodeList childNodes = root.getChildNodes();
            if(childNodes != null){
                for (int k=0; k<childNodes.getLength(); k++){
                    Node nd = childNodes.item(k);
                    if( nd != null ){
                        // A special case could be made for each Node type.
                        if( nd.getNodeType() == Node.ELEMENT_NODE )
                            dmtNode.add(fullTreeNode(nd));
                        
                        // This is the default
                        String data = nd.getNodeValue();
                        if(data != null){
                            data = data.trim();
                            if(!data.equals("\n") && !data.equals("\r\n") &&
                                    data.length() > 0){
                                dmtNode.add(fullTreeNode(nd));
                            }
                        }
                    }
                }
            }
        }
        return dmtNode;
    }
}