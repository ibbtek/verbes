# VERBES CHANGE LOG by Ibbtek

## Version 1.2

- Can launch program from command line with verb searched in argument
- Solve minor bugs
- Change way to scroll to current selected node tree
- Include read Help, About & Licenses file inside the *.jar

## Version 1.1

- Add Backward and Forward buttons
- Change the Tree construction and set it from a binary file to be faster
- Solve "j'" and "je" problem
- Add Linux and Mac capability to open default browser
- Add change display font to Arial 19
- Add "Edit" menu with copy/cut/paste submenu
